package com.pruebaTecnica.firebase

import com.pruebaTecnica.domain.model.Ubicacion

interface FirebaseInterface {
    fun onSuccess()
    fun Error()
    fun onSuccess(list: MutableList<Ubicacion>)
}