package com.pruebaTecnica.firebase

import android.app.Application
import android.location.Location
import android.net.Uri
import android.os.Build
import android.util.Log
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.firestore
import com.google.firebase.storage.storage
import com.pruebaTecnica.domain.model.Ubicacion
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date


class FirebaseServer (val interf : FirebaseInterface?, val application: Application) {
    var storage = Firebase.storage;

     fun subirImagene( imgen: Uri) {
        val storageRef = storage.reference
         val openInputStream = application.contentResolver.openInputStream(imgen)
         val date = Date()

        val mountainsRef = storageRef.child("IMG_"+date.time)

         if (openInputStream != null) {
             var uploadTask = mountainsRef.putStream(openInputStream!!)
             uploadTask.addOnFailureListener {
                 interf?.Error()
             }.addOnSuccessListener { taskSnapshot ->
                 interf?.onSuccess()
             }
         }

    }
    fun getUbicacion(){
        val db = Firebase.firestore
        var list: MutableList<Ubicacion> = mutableListOf()
        db.collection("Mapa")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val data = document.data
                    val fecha = data.get("fecha").toString()
                    val latitude = data.get("latitude").toString()
                    val longitude = data.get("longitude").toString()

                    val ubicacion = Ubicacion(fecha, latitude.toDouble(), longitude.toDouble())
                    list.add(ubicacion)
                    interf?.onSuccess(list)
                }
            }
            .addOnFailureListener { exception ->
                interf?.Error()
            }
    }
    fun guardarUbicacion(location:Location?){
        val db = Firebase.firestore
        var formattedDateTime =""

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val ofPattern = DateTimeFormatter.ofPattern("dd/MM/yyyy")
            val now = LocalDateTime.now()
             formattedDateTime = now.format(ofPattern)
        } else {
            
        }

        var id = db.collection("Mapa").document().id
        val ubicacion = Ubicacion(formattedDateTime,location!!.latitude , location!!.longitude);
        db.collection("Mapa").document(id)
            .set(ubicacion)
            .addOnSuccessListener { Log.d("TAG", "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w("TAG", "Error writing document", e) }

    }
}