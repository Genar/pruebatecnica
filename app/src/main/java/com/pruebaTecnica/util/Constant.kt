package com.pruebaTecnica.util

import com.pruebaTecnica.domain.model.Filtro

object Constant {
    val initialFiltros = mutableListOf(
        Filtro(1, "Mas popular", "popular"),
        Filtro(2, "Mas calificadas", "top_rated"),
        Filtro(1, "Proximos","upcoming")
    )
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val ACCOUNT = "b440974dd91d4abc4fe96449ebb15fd4"
    const val TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiNDQwOTc0ZGQ5MWQ0YWJjNGZlOTY0NDllYmIxNWZkNCIsInN1YiI6IjY1YjgzZGY3MjIzZTIwMDE3Yzc2YWRiNSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BNXVYagsR084aSpRwXkbcRJ4oAl9LyzbbDvPbgVxWKA"
}
