package com.pruebaTecnica

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PruebaApp: Application(){
    companion object{
        const val CHANEL_ID ="Notification"
    }
    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificaction()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificaction(){
        val chanel = NotificationChannel(
            CHANEL_ID,
            "Enviando ubicaciones",
            NotificationManager.IMPORTANCE_HIGH

        )
        val systemService = this.getSystemService(NotificationManager::class.java)
        systemService.createNotificationChannel(chanel)

    }
}