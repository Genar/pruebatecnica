package com.pruebaTecnica.domain

import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.model.Movie
import retrofit2.http.Path

interface Repository {
    suspend fun getAccount(account: String): Account?
    suspend fun getMovie(clasificacion: String,pagina: String ):Movie?
}