package com.pruebaTecnica.domain.model

data class AccountDetailState (
    var mostrarError : Boolean = false,
    var mostrarProgress : Boolean = false,
    var mensajeProgress : String = "",

    var mensajeError : String = "",
    var account: Account ?= null,
    var movie: Movie ?= null ,
    var list: MutableList<Ubicacion> = mutableListOf()

)