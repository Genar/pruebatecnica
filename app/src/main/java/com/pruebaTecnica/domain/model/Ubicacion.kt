package com.pruebaTecnica.domain.model

data class Ubicacion(var fecha: String, var latitude: Double, var longitude: Double)