package com.pruebaTecnica.domain.model


data class Movie (

    var page : Int = 0,
    var results: MutableList<Results> = mutableListOf<Results>()

)
data class Results(
    var id: Int = 0,
    val backdrop_path: String,
    var original_title: String?,
    var overview: String,
    var release_date: String,
    var title: String)