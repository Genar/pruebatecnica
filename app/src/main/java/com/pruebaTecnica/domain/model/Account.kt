package com.pruebaTecnica.domain.model


data class Account(
    val username: String,
    val include_adult: Boolean,
    val name: String,
   val id: String,
    val avatar: Avatar )

data class Avatar(
    var gravatar: Hash ,
    var tmdb : Tmdb
)
data class  Hash(
    val  hash: String
)
data class Tmdb(
    var avatar_path: String
)