package com.pruebaTecnica.domain.model

data class Filtro(var id : Int, var nombre: String, var tag : String)