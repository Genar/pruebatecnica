package com.pruebaTecnica.domain.useCase


import com.pruebaTecnica.data.dao.AccountDao
import com.pruebaTecnica.domain.Repository
import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.util.Util
import javax.inject.Inject

class GetAccount @Inject constructor(private val repository: Repository, private val dao: AccountDao,  private val util: Util) {
    suspend operator fun invoke(accout: String) : Account ? {

        if (util.conexionInternet()) {
            val account = repository.getAccount(accout);
            if (account != null)
                 dao.insertAccount(account)
            return  account

        }else {
            return dao.getAccout();
        }
        return null
    }
}