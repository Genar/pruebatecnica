package com.pruebaTecnica.domain.useCase

import com.pruebaTecnica.data.dao.MovieDao
import com.pruebaTecnica.domain.Repository
import com.pruebaTecnica.domain.model.Movie
import com.pruebaTecnica.util.Util
import javax.inject.Inject

class GetMovie  @Inject constructor(private val repository: Repository, private val dao: MovieDao, private val util: Util) {

    suspend operator fun invoke(clasificacion: String,pagina: String, tipo: Int ) : Movie? {

        if (util.conexionInternet()) {
            println("titulo buscando " +  clasificacion )
            val account = repository.getMovie(clasificacion,pagina )
            if (account != null)
                dao.insertMovie(account, tipo)
            return  account
        }else {
            return dao.getMovies();
        }
        return null
    }
}