package com.pruebaTecnica.ui.viewModel

import android.app.Application
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pruebaTecnica.PruebaApp
import com.pruebaTecnica.R
import com.pruebaTecnica.data.network.LocationService
import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.model.AccountDetailState
import com.pruebaTecnica.domain.useCase.GetAccount
import com.pruebaTecnica.firebase.FirebaseServer
import com.pruebaTecnica.util.Constant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import okhttp3.internal.notify
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(private val getAccount: GetAccount, var locationService: LocationService, val application: Application) : ViewModel() {

    private var _stateAccount = MutableStateFlow<AccountDetailState>( AccountDetailState())
    val stateAccout: StateFlow<AccountDetailState> = _stateAccount.asStateFlow()


 init {
     getAccount()
 }
    fun getAccount(){
        viewModelScope.launch {
            mostrarProgress(true)
            val result = getAccount(Constant.ACCOUNT)
            if(result!=null){
                mostrarProgress(false)
                actualizarAccount(result)
            }else{
                mostrarProgress(false)
                _stateAccount.value.mostrarError = true
                _stateAccount.value.mensajeError = ""
            }
        }

    }
    fun  obtenerUbicacion(){
        viewModelScope.launch {

            val location = locationService.getLocation()
            val firebase = FirebaseServer(null, application)
            firebase.guardarUbicacion(location)
            enviarNotificacion()
        }

    }
    fun enviarNotificacion(){
        val systemService = application.getSystemService(NotificationManager::class.java)
        val notification = NotificationCompat.Builder(application, PruebaApp.CHANEL_ID)
            .setContentText(application.getText(R.string.mensaje_notificacion))
            .setContentTitle(application.getText(R.string.mensaje_titulo))
            .setSmallIcon(R.drawable.ic_notification)
            .setAutoCancel(true)
            .build()

        systemService.notify(1,notification)

    }
    fun mostrarProgress(mostrar: Boolean){
        _stateAccount.update { currentState ->
            currentState.copy(
                mostrarProgress= mostrar
            )
        }

    }
    fun actualizarAccount(account: Account){
        _stateAccount.update { currentState ->
            currentState.copy(
               account = account
            )
        }
    }
}