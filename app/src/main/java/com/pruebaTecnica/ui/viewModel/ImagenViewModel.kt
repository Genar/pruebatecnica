package com.pruebaTecnica.ui.viewModel

import android.app.Application
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pruebaTecnica.R
import com.pruebaTecnica.domain.model.AccountDetailState
import com.pruebaTecnica.domain.model.Ubicacion
import com.pruebaTecnica.firebase.FirebaseInterface
import com.pruebaTecnica.firebase.FirebaseServer
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImagenViewModel @Inject constructor(var application: Application) : ViewModel() , FirebaseInterface {
    private var _state = MutableStateFlow<AccountDetailState>( AccountDetailState())
    val state: StateFlow<AccountDetailState> = _state.asStateFlow()

    fun guardarImagenes(  images: List<Uri>){
        if (images != null && images.size > 0) {
            val firebase = FirebaseServer(this, application)
            viewModelScope.launch {
                mostrarProgress(true, application.getString(R.string.mensaje_consultando))
                for (uri in images) {
                    application.contentResolver.takePersistableUriPermission(
                        uri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION
                    )
                    firebase.subirImagene(uri)
                }

            }
        }else {
            clickOcultar(true, application.getString(R.string.mensaje_imagen))
        }
    }

    fun mostrarProgress(mostrar: Boolean,mensaje:String){
        _state.update { currentState ->
            currentState.copy(
                mostrarProgress= mostrar,
                mensajeProgress =  mensaje
            )
        }

    }

    override fun onSuccess() {
        clickOcultar(true, application.getString(R.string.mensaje_exito))
        mostrarProgress(false, "")
    }

    override fun onSuccess(list: MutableList<Ubicacion>) {

    }

    override fun Error() {
        clickOcultar(true, application.getString(R.string.mensaje_error))
        mostrarProgress(false, "")
    }
    fun clickOcultar(mostrar : Boolean, mensaje: String){
        _state.update { currentState ->
            currentState.copy(
                mostrarError = mostrar,
                mensajeError = mensaje
            )
        }
    }

}