package com.pruebaTecnica.ui.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import com.pruebaTecnica.R
import com.pruebaTecnica.domain.model.AccountDetailState
import com.pruebaTecnica.domain.model.Ubicacion
import com.pruebaTecnica.firebase.FirebaseInterface
import com.pruebaTecnica.firebase.FirebaseServer
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject
@HiltViewModel
class MapsViewModel@Inject constructor(var application: Application) : ViewModel(),
    FirebaseInterface {
    private var _state = MutableStateFlow<AccountDetailState>( AccountDetailState())
    val state: StateFlow<AccountDetailState> = _state.asStateFlow()

    init {
        getUbicacion()
    }
    fun getUbicacion(){
        val firebase = FirebaseServer(this, application)
        firebase.getUbicacion()
    }

    fun mostrarProgress(mostrar: Boolean,mensaje:String){
        _state.update { currentState ->
            currentState.copy(
                mostrarProgress= mostrar,
                mensajeProgress =  mensaje
            )
        }

    }

    override fun onSuccess() {
        clickOcultar(true, application.getString(R.string.mensaje_exito))
        mostrarProgress(false, "")
    }

    override fun onSuccess(list: MutableList<Ubicacion>) {
        actualizarlista(list)
    }
    fun actualizarlista(list: MutableList<Ubicacion>){
        _state.update { currentState ->
            currentState.copy(
              list = list
            )
        }
    }
    override fun Error() {
        clickOcultar(true, application.getString(R.string.mensaje_error))
        mostrarProgress(false, "")
    }
    fun clickOcultar(mostrar : Boolean, mensaje: String){
        _state.update { currentState ->
            currentState.copy(
                mostrarError = mostrar,
                mensajeError = mensaje
            )
        }
    }
}