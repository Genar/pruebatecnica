package com.pruebaTecnica.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.model.AccountDetailState
import com.pruebaTecnica.domain.model.Movie
import com.pruebaTecnica.domain.useCase.GetAccount
import com.pruebaTecnica.domain.useCase.GetMovie
import com.pruebaTecnica.util.Constant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class MovieViewModel @Inject constructor(private val getMovie: GetMovie) : ViewModel()  {
    private var _stateMovie = MutableStateFlow<AccountDetailState>( AccountDetailState())
    val stateMovie: StateFlow<AccountDetailState> = _stateMovie.asStateFlow()

    init {
        getMovies("popular","0", 1)
    }
    fun getMovies(clasificacion: String,pagina: String, tipo: Int ){
        viewModelScope.launch {
            mostrarProgress(true)
            val result = getMovie(clasificacion,pagina, tipo)
            if(result!=null){
                mostrarProgress(false)
                println("titulo " + result.results.get(0).title)
                actualizarAccount(result)
            }else{
                println("titulo no " )
                mostrarProgress(false)
                _stateMovie.value.mostrarError = true
                _stateMovie.value.mensajeError = ""
            }
        }

    }
    fun mostrarProgress(mostrar: Boolean){
        _stateMovie.update { currentState ->
            currentState.copy(
                mostrarProgress= mostrar
            )
        }

    }
    fun actualizarAccount(movie: Movie){
        _stateMovie.update { currentState ->
            currentState.copy(
                movie = movie
            )
        }
    }
}