package com.pruebaTecnica.ui.screem

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.pruebaTecnica.R
import com.pruebaTecnica.domain.model.Results
import com.pruebaTecnica.ui.viewModel.MovieViewModel
import com.pruebaTecnica.util.Constant

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieeScreem(model : MovieViewModel = viewModel()){

    val uiState by model.stateMovie.collectAsState();
    val filtros = Constant.initialFiltros
    var expanded by remember { mutableStateOf(false) }
    var selectedText by remember { mutableStateOf(filtros[0]) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(32.dp)
    ) {
        Column(Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally) {
            ExposedDropdownMenuBox(
                expanded = expanded,
                onExpandedChange = {
                    expanded = !expanded
                }
            ) {
                TextField(
                    value = selectedText.nombre,
                    onValueChange = {},
                    readOnly = true,
                    trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                    modifier = Modifier.menuAnchor()
                )

                ExposedDropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    filtros.forEach { item ->
                        DropdownMenuItem(
                            text = { Text(text = item.nombre) },
                            onClick = {
                                selectedText = item
                                expanded = false
                               model.getMovies(item.tag, "0", item.id)
                            }
                        )
                    }
                }
            }
            Column (
                Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp, bottom = 16.dp)
                    .verticalScroll(rememberScrollState())){

                val results = uiState.movie?.results
                if (results != null) {
                    print("long " + results.size)
                    for (resul in results) {
                        Item(resul)
                    }
                }
                Divider(color = Color.Black, thickness = 1.dp)
            }


        }
        if (uiState.mostrarProgress){
            Progress(stringResource(id = R.string.mensaje_consultando))
        }
    }
}
@Composable
fun Item(movies: Results){
    Column(
        Modifier
            .fillMaxWidth()
            .padding(top = 8.dp, bottom = 8.dp)) {
        Divider(color = Color.Black, thickness = 1.dp)
        Row(Modifier.fillMaxWidth()){

            AsyncImage(
                model = "https://image.tmdb.org/t/p/w220_and_h330_face/"+ movies.backdrop_path,
                error = painterResource(id = R.drawable.ic_perfil),
                contentDescription = "The delasign logo",
            )
            Column (Modifier.padding(start = 16.dp, end =  16.dp)){
                Text(text = movies.title, color = Color.Blue)
                Text(text = movies.overview)

            }

        }
        Divider(color = Color.Black, thickness = 1.dp)

    }

}