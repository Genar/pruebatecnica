package com.pruebaTecnica.ui.screem

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.pruebaTecnica.R
import com.pruebaTecnica.ui.viewModel.ProfileViewModel


@Composable
fun TabScreen() {

    var tabIndex by remember { mutableStateOf(0) }

    val tabs = listOf(stringResource(R.string.tab_profiel), stringResource(R.string.tab_movie), stringResource(R.string.tab_ubi), stringResource(R.string.tab_image))

    Column(modifier = Modifier.fillMaxWidth()) {
        TabRow(selectedTabIndex = tabIndex) {
            tabs.forEachIndexed { index, title ->
                Tab(text = { Text(title) },
                    selected = tabIndex == index,
                    onClick = { tabIndex = index }
                )
            }
        }
        when (tabIndex) {
           0-> ProfileScreem()
            1-> MovieeScreem()
            2->Maps()
            3->ImagenScreem()
        }
    }
}