package com.pruebaTecnica.ui.screem

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.pruebaTecnica.R
import com.pruebaTecnica.ui.viewModel.ImagenViewModel
import kotlinx.coroutines.launch

@Composable
fun ImagenScreem (  model : ImagenViewModel = viewModel()){
    val uiState by model.state.collectAsState();
    var imageUriList = remember {
        mutableStateOf<List<Uri>>(emptyList())
    }
    val context = LocalContext.current

    var imageUri: Uri? by remember { mutableStateOf(null) }


    val scope = rememberCoroutineScope()


    val galleryLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.GetMultipleContents()) {
            print("imagenes " +  it.size)
            imageUriList.value  = it



        }
    if (uiState.mostrarProgress){
        Progress(uiState.mensajeProgress)
    }
    if (uiState.mostrarError) {
        AlertaError({ model.clickOcultar(false,"") }, {model.clickOcultar(false, "")}, uiState.mensajeError)
    }
    Column (Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
        Row {
            Button(onClick = {
                galleryLauncher.launch("image/*") }) {
                Text(
                    text = stringResource(id = R.string.seleccionar_imagne)
                )
            }
            Button(onClick = {
                model.guardarImagenes(imageUriList.value)}) {
                Text(
                    text =  stringResource(id = R.string.guardar)
                )
            }
        }

        Row {
            repeat(imageUriList.value.size) { index ->
                Image(
                    painter = rememberAsyncImagePainter(
                        model = imageUriList.value[index]
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .clip(CircleShape)
                        .size(100.dp)
                )
            }
        }
    }
}