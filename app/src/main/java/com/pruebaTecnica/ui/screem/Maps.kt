package com.pruebaTecnica.ui.screem

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import com.pruebaTecnica.ui.viewModel.MapsViewModel

@Composable
fun Maps( model : MapsViewModel = viewModel()) {
    val uiState by model.state.collectAsState();

    val marker= LatLng(20.663999,-100.315552)
    val marker2= LatLng(10.663999,-100.315552)
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(marker, 10f)
    }

        GoogleMap(
            Modifier.fillMaxSize(),
            cameraPositionState = cameraPositionState
        ) {
            for (coord in uiState.list) {
                Marker(
                    state = MarkerState(
                        position = LatLng(
                            coord.latitude.toDouble(),
                            coord.longitude.toDouble()
                        )
                    ),
                    title = coord.fecha
                )
            }

        }

}