package com.pruebaTecnica.ui.screem


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.pruebaTecnica.R
@Composable
fun AlertaError(acpetar: () -> Unit,cancelar: () -> Unit, mensajeAlert : String ?) {

    AlertDialog(
        onDismissRequest = {

        },
        confirmButton = {
            Button(onClick = { acpetar() },
                Modifier.fillMaxWidth(),
                colors = ButtonDefaults.outlinedButtonColors(colorResource(id = R.color.blue))) {
                Text(
                    text = stringResource(id = R.string.aceptar),
                    fontSize = 20.sp,
                    color = colorResource(id = R.color.white)
                )
            }
        },
        title = {
            Text(
                text = stringResource(id = R.string.app_name),
                fontSize = 25.sp,
                color = colorResource(id = R.color.blue),
                fontWeight = FontWeight.Bold
            )
        },
        text = {
            Column(
                Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {


                Text(
                    text = mensajeAlert!!,
                    fontSize = 20.sp
                )
            }

        },
        containerColor = Color.White
    )
}