package com.pruebaTecnica.ui.screem

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.pruebaTecnica.R
import com.pruebaTecnica.ui.viewModel.ProfileViewModel

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun ProfileScreem(model: ProfileViewModel = viewModel()){
    val uiState by model.stateAccout.collectAsState();
     val neededPermissions = listOf<String>(
         android.Manifest.permission.ACCESS_COARSE_LOCATION,
         android.Manifest.permission.ACCESS_FINE_LOCATION,
         android.Manifest.permission.POST_NOTIFICATIONS
    )

    val permission = rememberMultiplePermissionsState(neededPermissions)
    LaunchedEffect(true){
        permission.launchMultiplePermissionRequest()
    }


    var granted : Boolean = false
    for (permisision in permission.permissions){
        granted = permisision.status.isGranted
    }
    if (granted){
        model.obtenerUbicacion()
    }


    Column(
       Modifier
           .fillMaxWidth()
           .padding(16.dp),
       horizontalAlignment = Alignment.CenterHorizontally) {
       AsyncImage(
           model = "https://image.tmdb.org/t/p/w150_and_h150_face/"+ uiState.account?.avatar?.tmdb?.avatar_path ?:  "",
           error = painterResource(id = R.drawable.ic_perfil),
           contentDescription = "The delasign logo",
       )

       Text(text = (uiState.account?.name ?: ""),
           fontSize = 18.sp,
           color = colorResource(id = R.color.black))

       Text(text = stringResource(id = R.string.user),
           fontSize = 22.sp,
           color = colorResource(id = R.color.blue))
       Text(text = (uiState.account?.username ?: ""),
           fontSize = 18.sp,
           color = colorResource(id = R.color.black))

   }
    if (uiState.mostrarProgress){
        Progress(stringResource(id = R.string.mensaje_consultando))
    }

}