package com.pruebaTecnica.data.entityRealm

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject

class MovieRealm : RealmObject{
    var tipo: Int = 0;
    var page : Int = 0
    var results: RealmList<ResultsRealm> = realmListOf()
}
class ResultsRealm: RealmObject{
    var id : Int = 0;
    var backdrop_path : String ?= ""
    var original_title: String ?= ""
    var overview: String ?= ""
    var release_date: String ?= ""
    var title : String ?= ""
}