package com.pruebaTecnica.data.entityRealm

import io.realm.kotlin.types.RealmObject

class AccountRealm : RealmObject{
    var username: String = ""
    var include_adult: Boolean = false
    var name: String = ""
    var id: String = ""
    var avatar: AvatarRealm?= null

}
class AvatarRealm: RealmObject {
    var gravatar: HashRealm? = null
    var tmdb: TmdbRealm? = null
}
class  HashRealm : RealmObject {

    var hash: String ?= ""
}
class TmdbRealm: RealmObject{
    var avatar_path: String ?= null
}
