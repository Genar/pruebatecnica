package com.pruebaTecnica.data

import com.pruebaTecnica.data.network.ApiServer
import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.Repository
import com.pruebaTecnica.domain.model.Movie
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val apiService: ApiServer) : Repository {

    override suspend fun getAccount(account: String): Account ? {
        runCatching { apiService.getAccount(account) }
            .onSuccess { return it.toAoccount()}
            .onFailure {  return null}
        return  null
    }

    override suspend fun getMovie(clasificacion: String, pagina: String): Movie? {
        runCatching {
            apiService.getMovies(clasificacion) }
            .onSuccess { return it.toAoccount()}
            .onFailure {  return null}
        return  null
    }


}