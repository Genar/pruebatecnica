package com.pruebaTecnica.data.dao

import com.pruebaTecnica.data.entityRealm.MovieRealm
import com.pruebaTecnica.data.entityRealm.ResultsRealm
import com.pruebaTecnica.domain.model.Movie
import com.pruebaTecnica.domain.model.Results
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieDao @Inject constructor(val realm: Realm) {

    fun insertMovie(movie: Movie, cate: Int){
        GlobalScope.launch {
            realm?.write {
                this.copyToRealm(MovieRealm().apply {
                    tipo = cate
                    page = movie.page
                    val movies = movie.results;
                    if (movies != null){
                        for ( movie in movies){
                            var resultsRealm = ResultsRealm()
                            resultsRealm.backdrop_path = movie.backdrop_path
                            resultsRealm.original_title = movie.original_title
                            resultsRealm.overview = movie.overview
                            resultsRealm.title = movie.title
                            resultsRealm.release_date = movie.release_date
                            results.add(resultsRealm)
                        }
                    }

                })
            }
        }
    }
    fun getMovies(): Movie{
        val find = realm?.query<MovieRealm>()?.first()?.find()
        var movie: Movie = Movie()
        if (find != null){
            movie.page = find.page
            if (find.results != null) {
               var list :  MutableList<Results> = mutableListOf()
                var element : Results
                for (result in find.results) {

                    element = Results(result?.id ?: 0,result?.backdrop_path?: "", result?.original_title, result?.overview?: "", result?.release_date?: "", result?.title?: "")
                    list.add(element)
                }
                movie.results = list
            }
        }
        return movie
    }

}