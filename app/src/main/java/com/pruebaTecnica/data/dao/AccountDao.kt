package com.pruebaTecnica.data.dao

import com.pruebaTecnica.data.entityRealm.AccountRealm
import com.pruebaTecnica.data.entityRealm.AvatarRealm
import com.pruebaTecnica.data.entityRealm.HashRealm
import com.pruebaTecnica.data.entityRealm.TmdbRealm


import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.model.Avatar
import com.pruebaTecnica.domain.model.Hash
import com.pruebaTecnica.domain.model.Tmdb


import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountDao @Inject constructor(val realm: Realm) {

    fun insertAccount(account: Account){
        eliminarAccount()
        GlobalScope.launch {
            realm?.write {
                this.copyToRealm(AccountRealm().apply {
                    var hash = HashRealm()
                    hash.hash =account.avatar?.gravatar?.hash ?: ""
                    var tmd = TmdbRealm()
                    tmd.avatar_path = account.avatar?.tmdb?.avatar_path ?: ""
                    var av = AvatarRealm()
                    av.tmdb = tmd
                    av.gravatar =hash

                    username = account.username
                    include_adult = account.include_adult
                    name = account.name
                    id= account.id
                    avatar = av

                })
            }
        }
    }
    fun eliminarAccount(){
        val   find = realm?.query<AccountRealm>()?.first()?.find()
        // delete one object synchronously
        realm.writeBlocking {
            if (find != null) {
                findLatest(find)
                    ?.also { delete(it) }
            }
        }

    }
    fun getAccout(): Account ?{
        var account: Account ?= null
        val find = realm?.query<AccountRealm>()?.first()?.find()
        if (find != null){

            var hash  = Hash(find.avatar?.gravatar?.hash ?: "")
            var tdm = Tmdb(find.avatar?.tmdb?.avatar_path ?: "")
            var avatar =  Avatar(hash, tdm)
            account = Account(find.username, find.include_adult, find.name,find.id, avatar);
        }
       return account;
    }
}