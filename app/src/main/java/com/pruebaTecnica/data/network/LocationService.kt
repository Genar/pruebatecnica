package com.pruebaTecnica.data.network

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.compose.runtime.LaunchedEffect
import androidx.core.app.ActivityCompat
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject

class LocationService @Inject constructor(private val aplication: Application)  {
    @SuppressLint("MissingPermission")
    suspend fun getLocation():Location?{
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(aplication)


        val locationManager = aplication.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val isGPSEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        if (!isGPSEnable){
            return  null
        }
        return suspendCancellableCoroutine {
            con->
            fusedLocationProviderClient.lastLocation.apply {
                if(isComplete){
                    if(isSuccessful){
                        con.resume(result){}
                    }else {
                        con.resume(null){}
                    }

                }
                addOnSuccessListener {
                    con.resume(result){}
                }
                addOnCanceledListener { con.resume(null){} }
                addOnFailureListener { con.resume(null){} }
            }
        }
    }
}