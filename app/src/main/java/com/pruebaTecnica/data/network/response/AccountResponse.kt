package com.pruebaTecnica.data.network.response

import com.google.gson.annotations.SerializedName
import com.pruebaTecnica.domain.model.Account
import com.pruebaTecnica.domain.model.Avatar

data class AccountResponse(
   @SerializedName("username") val username: String,
   @SerializedName("include_adult")  val include_adult: Boolean,
   @SerializedName("name")   val name: String,
   @SerializedName("id")  val id: String,
   @SerializedName("avatar") val avatar: Avatar)

{
   fun toAoccount(): Account {
       val avatar1 = Avatar(
           gravatar = avatar.gravatar,
           tmdb = avatar.tmdb
       )
       return Account(
        username = username,
         include_adult = include_adult,
         name = name,
         id = id,
          avatar = avatar1
      )
   }
}
data class Avatar(
    @SerializedName("gravatar")  val gravatar: hash ?= null,
    @SerializedName("tmdb") var tmdb : Tmdb ?= null
)
data class  hash(
    @SerializedName("hash")   val  hash: String
)
data class Tmdb(
    @SerializedName("avatar_path")   val avatar_path: String ?= null
)