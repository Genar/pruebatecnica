package com.pruebaTecnica.data.network

import com.pruebaTecnica.data.network.response.AccountResponse
import com.pruebaTecnica.data.network.response.MovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface ApiServer {
    @GET("account/{cuenta}")
    suspend fun getAccount( @Path("cuenta") cuenta: String): AccountResponse

    @GET("movie/{clasificacion}")
    suspend fun getMovies(@Path("clasificacion") clasificacion:String): MovieResponse

}