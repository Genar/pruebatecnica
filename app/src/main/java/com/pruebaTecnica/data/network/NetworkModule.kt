package com.pruebaTecnica.data.network

import android.app.Application
import android.content.Context
import com.pruebaTecnica.data.RepositoryImpl
import com.pruebaTecnica.data.dao.AccountDao
import com.pruebaTecnica.data.entityRealm.AccountRealm
import com.pruebaTecnica.data.entityRealm.AvatarRealm
import com.pruebaTecnica.data.entityRealm.HashRealm
import com.pruebaTecnica.data.entityRealm.MovieRealm
import com.pruebaTecnica.data.entityRealm.ResultsRealm
import com.pruebaTecnica.data.entityRealm.TmdbRealm
import com.pruebaTecnica.domain.Repository
import com.pruebaTecnica.util.Constant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(Constant.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    @Provides
    @Singleton
    fun httpClient():OkHttpClient{
        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS)

        return OkHttpClient
            .Builder()
            .addInterceptor(Interceptor(){
                var newRequest = it.request().newBuilder()
                    .addHeader("Authorization","Bearer ${Constant.TOKEN}")
                    .build()
                it.proceed(newRequest)
            })
            .build()
    }
    @Provides
    fun provideAccont(retrofit: Retrofit): ApiServer {
        return retrofit.create(ApiServer::class.java)
    }

    @Provides
    fun provideRepository(apiService: ApiServer): Repository {
        return RepositoryImpl(apiService)
    }
    @Provides
    fun getReaml() : Realm {
        val configuration = RealmConfiguration.Builder(
            setOf(
                AccountRealm::class, AvatarRealm::class, HashRealm::class, TmdbRealm::class,
                ResultsRealm::class, MovieRealm::class
            ))
            .name("autTerceros")
            .schemaVersion(1)
            .build()
        val realm = Realm.open(configuration = configuration)
        return realm
    }
    @Provides
    fun providerModule(realm: Realm):AccountDao{
        return AccountDao(realm)
    }

    @Provides
    fun provideContext(application: Application): Context = application.applicationContext

}