package com.pruebaTecnica.data.network.response

import com.google.gson.annotations.SerializedName
import com.pruebaTecnica.domain.model.Movie
import com.pruebaTecnica.domain.model.Results

data class MovieResponse (
    @SerializedName("page")  val page : Int,
    @SerializedName("results")  val results: List<ResultsRespose>
){
    fun toAoccount(): Movie {
        var list : MutableList<Results> = mutableListOf()
        if (results != null) {

            for (resul in results) {
                var rsul = Results(
                    backdrop_path = resul.backdrop_path,
                    original_title = resul.original_title,
                    overview = resul.overview,
                    release_date = resul.release_date,
                    title = resul.title
                )
                list.add(rsul)
            }
        }
       var movie = Movie(page =  page,list )
        return  movie;
    }
}

data class ResultsRespose(
    @SerializedName("backdrop_path")  val backdrop_path : String,
    @SerializedName("original_title") val original_title: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val release_date: String,
    @SerializedName("title")  val title : String
)



